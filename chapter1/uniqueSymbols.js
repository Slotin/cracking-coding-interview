function uniqueSymbols(str) {
    const strArray = str.split('');
    let isUnique = true;

    strArray.sort((a, b) => a > b);

    for (let i = 0; i < strArray.length; i++) {
        if (strArray[i] === strArray[i + 1]) {
            isUnique = false;
        }
    }

    return isUnique;
}

function uniqueSymbolsUsingSet(str) {
    const strArray = str.split('');
    const stringSet = new Set();

    for (let i = 0; i < strArray.length; i++) {
        stringSet.add(strArray[i]);
    }

    return stringSet.size === strArray.length;
}

console.log(uniqueSymbolsUsingSet('avcfdfbn'));