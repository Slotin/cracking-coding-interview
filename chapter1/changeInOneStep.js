function checkStepsCondition(steps) {
    return steps <= 1;
}

function changeInOneStep(str1, str2) {
    let steps = 0;
    const firstArr = str1.split('');
    const secondArr = str2.split('');
    steps = Math.abs(firstArr.length - secondArr.length);

    if (steps > 0 ) {
        return checkStepsCondition(steps)
    }

    for (let i = 0;  i < firstArr.length; i++) {
        if (firstArr[i] !== secondArr[i]) {
            steps++;
        }
    }

    return checkStepsCondition(steps);
}

console.log(changeInOneStep('pale', 'ple'));
console.log(changeInOneStep('pales', 'pale'));
console.log(changeInOneStep('pale', 'bale'));
console.log(changeInOneStep('pale', 'bake'));