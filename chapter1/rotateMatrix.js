function rotateMatrix (matrix, n) {
  for (let layer = 0; layer < n/2; ++layer) {
    let first = layer;
    let last = n - 1 - layer;
    
    for (let i = first; i < last; ++i) {
      let offset = i - first;
      let top = matrix[first][i];
      // console.log(offset, top, last);

      // console.log('first operation', matrix[first][i], matrix[last - offset][first])
      matrix[first][i] = matrix[last - offset][first];
      // console.log('second operation', matrix[last - offset][first], matrix[last][last - offset])
      matrix[last - offset][first] = matrix[last][last - offset];
      // console.log('third operation', matrix[last][last - offset], matrix[i][last]);
      matrix[last][last - offset] = matrix[i][last];
      // console.log('fourth operation', matrix[i][last], top);
      matrix[i][last] = top;

      // console.log('\n')
    }
  }
  console.log(matrix);
}

rotateMatrix([
  [1,2,3,10],
  [4,5,6,11],
  [7,8,9,12],
  [13,14,15,16]
], 4);