function nullMatrixElement (matrix, m, n) {
  const naullable = [];
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < m; j++) {
      if (matrix[i][j] === 0) {
        naullable.push({
          row: i,
          column: j
        });
      }
    }
  }

  for (let k = 0; k < naullable.length; k++) {
    matrix[naullable[k].row] = new Array(m).fill(0);
    for (let z = 0; z < n; z++) {
      for (let x = 0; x < m; x++) {
        if (naullable[k].column === x) {
          matrix[z][x] = 0;
        }
      }
    }
  }
  return matrix;
}

// console.log(nullMatrixElement([
//   [4, 0, 3, 4],
//   [2, 3, 5, 4],
//   [6, 2, 4, 3]
// ], 4, 3));

console.log(nullMatrixElement([
  [4, 5, 1, 3],
  [2, 0, 5, 0],
  [6, 2, 2, 3],
  [5, 6, 8, 1],
  [12, 42, 22, 0]
], 4, 5));