function replaceSpaces (str, strLimit) {
    const strArr = str.split('');

    for (let i = 0; i <= strArr.length; i++) {
        strArr[i] === ' ' && (strArr[i] = '%20');
    }

    strLimit > strArr.length && (strLimit = strArr.length);

    return strArr.slice(0, strLimit).join('');
}

console.log(replaceSpaces('Mr John Smith    ', 13));