const LinkedList = require('./LinkedList');

const firstElement = new LinkedList('a');
const secondElement = new LinkedList('b');
const thirdElement = new LinkedList('c');
const fourthElelemt = new LinkedList('d');
const fifthElelemt = new LinkedList('e');

firstElement.next = secondElement;
secondElement.next = thirdElement;
thirdElement.next = fourthElelemt;
fourthElelemt.next = fifthElelemt;

const deleteElement = (element) => {
  if (element == null || element.next == null) {
    return false;
  }

  const next = {... element.next };
  element.value = next.value;
  element.next = next.next;
  return true;
}

console.log(deleteElement(thirdElement));

let elm = firstElement;
while (elm !== null) {
  console.log(elm.value);
  elm = elm.next;
}