const LinkedList = require('./LinkedList');

function printLinkedList (head) {
  let node = head;
  while (node !== null) {
    console.log(node.value);
    node = node.next
  }
}

function detectDoubles(head) {
  let currentNode = head;
  while (currentNode !== null) {
    let nextNode = currentNode.next;
    while (nextNode !== null) {
      if (currentNode.value === nextNode.value) {
        currentNode.next = nextNode.next;
      }

      nextNode = nextNode.next;
    }

    currentNode = currentNode.next;
  }

  return head;
}



const firstElement = new LinkedList('a');
const secondElement = new LinkedList('a');
const thirdElement = new LinkedList('c');
const fourthElelemt = new LinkedList('c');

firstElement.next = secondElement;
secondElement.next = thirdElement;
thirdElement.next = fourthElelemt;


console.log('original list \n');
printLinkedList(firstElement);

detectDoubles(firstElement);

console.log('modified list \n');
printLinkedList(firstElement);

