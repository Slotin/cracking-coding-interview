const LinkedList = require('./LinkedList');

const firstElement = new LinkedList('a');
const secondElement = new LinkedList('b');
const thirdElement = new LinkedList('c');
const fourthElelemt = new LinkedList('d');
const fifthElelemt = new LinkedList('e');

firstElement.next = secondElement;
secondElement.next = thirdElement;
thirdElement.next = fourthElelemt;
fourthElelemt.next = fifthElelemt;

const findkElement = (elementNumber, head) => {
  let listLength = 1;

  if (!head) {
    return null;
  } else if (head.next) {
    let node = head;

    while (node.next !== null) {
      listLength++;
      node = node.next;
    }
  }

  if (listLength > 0 && listLength >= elementNumber) {
    let iterateNumber = 1;
    let findingNodeIndex = listLength + 1 - elementNumber;
    let node = head;
    while (iterateNumber < findingNodeIndex) {
      node = node.next;
      iterateNumber++;
    }

    return node.value;
  } else {
    return null;
  }
}

const findkElementArray = (elementNumber, list) => {
  const arr = []
  if (!list) {
    return null;
  } else if (list.next) {
    let node = list;

    while (node.next !== null) {
      arr.push(node);
      node = node.next;
    }
    arr.push(node);

    return arr[arr.length - elementNumber].value;
  }
}

const findElemementParallel = (elementNumber, head) => {
  let p1 = head;
  let p2 = head;

  for (let i = 0; i < elementNumber; i++) {
    if (p1 === null) return null;
    p1 = p1.next;
  }

  while (p1 !== null) {
    p1 = p1.next;
    p2 = p2.next;
  }

  return p2.value;
}

console.log(findElemementParallel(2, firstElement));