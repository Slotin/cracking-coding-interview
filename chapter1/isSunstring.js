function isSubstring(str1, str2) {
  let isSub = true;

  if (str1.length !== str2.length) {
    return false;
  }

  const stringthLength = str1.length;

  const firstLetter = str1[0];
  const offset = str2.indexOf(firstLetter);

  if (offset === -1) {
    return false;
  }

  for (let i = 0; i < stringthLength; i++) {
    const currentOffset = i + offset > stringthLength - 1 ? (i + offset - stringthLength) : (i + offset);
    if (str1[i] !== str2[currentOffset]) {
      isSub = false;
    }
  }

  return isSub;
}

//should return false
console.log(isSubstring('abx', 'bxac'))

//should return true
console.log(isSubstring('acbx', 'bxac'))

//should return true
console.log(isSubstring('waterbottle', 'erbottlewat'))

//should return false
console.log(isSubstring('waterbottle', 'erbottlewac'))

//should return false
console.log(isSubstring('waterbottlx', 'erbottlewac'))