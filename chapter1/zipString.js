function zipString (str) {
    let prevLatter;
    let counter = 0;
    const outArray = [];
    for (let i = 0; i <= str.length; i++) {
        if (str[i] === prevLatter) {
            counter++;
        } else {
            if (prevLatter) {
                outArray.push(`${prevLatter}${counter}`)
            }
            prevLatter = str[i];
            counter = 1;
        }
    }

    return str.length === outArray.length ? str : outArray.join('');
}
//Should return zipped string
console.log(zipString('aaaasssddffddssss'));
//should return original string
console.log(zipString('abshdgklp'))