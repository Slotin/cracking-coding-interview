function sortFn (a, b) {
    return a < b;
}
function detectSimilarCharacters (str1, str2) {
    const first = str1.split('').sort(sortFn).join('');
    const second = str2.split('').sort(sortFn).join('');

    return first === second;
}

console.log(detectSimilarCharacters('acvd', 'dcaa'));