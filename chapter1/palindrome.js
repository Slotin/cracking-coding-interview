function palindrome(str) {
    const charNumbers = {};
    let isPalynome = true;
    let haveOneLetterUnique = false;

    str.split('').forEach(item => {
        const currentChar = item.toLowerCase();
        if (currentChar !== ' ') {
            if (charNumbers[currentChar] === undefined) {
                charNumbers[currentChar] = 0;
            }}

            charNumbers[currentChar]++
    });

    Object.keys(charNumbers).forEach(key => {
        if (charNumbers[key] %2 > 0) {
            if (haveOneLetterUnique) {
                isPalynome = false;
            } else {
                haveOneLetterUnique = true;
            }
        }
    });


    return isPalynome;
}

console.log(palindrome('Tact Coa'), 'true');
console.log(palindrome('Tact boa'), 'false');